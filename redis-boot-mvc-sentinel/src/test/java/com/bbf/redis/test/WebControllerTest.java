package com.bbf.redis.test;

import com.bbf.redis.Application;
import com.bbf.redis.controller.WebTestController;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.annotation.Resource;

/**
 * Controller测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
//指定SpringBoot工程的Application启动类
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.MOCK)
//Spring boot用来模拟ServletContext，并加载上下文
@WebAppConfiguration
//根据test方法名按照字典顺序升序排序
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WebControllerTest {
  @Resource
  private WebTestController controller;
  private MockMvc mvc;

  @Before
  public void setUp() throws Exception {
    mvc = MockMvcBuilders.standaloneSetup(controller).build();
  }

  private String key = "201705022";

  //测试查询
  @Test
  public void test_1_redis_set() throws Exception {
    String msg = "redis-boot-mvc-sentinel";
    String url = String.format("/redis/set/%s/%s", key, msg);
    MvcResult result = mvc.perform(
        MockMvcRequestBuilders.get(url)
            .accept(MediaType.APPLICATION_JSON))
        .andReturn();
    int statusCode = result.getResponse().getStatus();
    //设置断言，判断http状态
    Assert.assertEquals(200, statusCode);
    String body = result.getResponse().getContentAsString();
    System.out.println("response-body: " + body);
  }

  @Test
  public void test_2_redis_get() throws Exception {
    String url = String.format("/redis/get/%s", key);
    MvcResult result = mvc.perform(
        MockMvcRequestBuilders.get(url)
            .accept(MediaType.APPLICATION_JSON))
        .andReturn();
    int statusCode = result.getResponse().getStatus();
    //设置断言，判断http状态
    Assert.assertEquals(200, statusCode);
    String body = result.getResponse().getContentAsString();
    System.out.println("response-body: " + body);
  }

}
