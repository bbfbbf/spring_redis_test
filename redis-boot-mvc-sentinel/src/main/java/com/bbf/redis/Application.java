package com.bbf.redis;

import com.alibaba.fastjson.parser.ParserConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

/**
 * 入口类
 * @author BBF
 */
@SpringBootApplication
@ImportResource(locations = {"classpath:spring-conf-redis-sentinel.xml"})
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  /**
   * 添加FastJson的autoType白名单
   * @return ParserConfig
   * @see <a href="https://github.com/alibaba/fastjson/wiki/enable_autotype">添加autoType白名单</a>
   */
  @Bean
  public ParserConfig setFastJsonAutoType() {
    ParserConfig parserConfig = ParserConfig.getGlobalInstance();
    //如果有多个包名前缀，分多次addAccept
    parserConfig.addAccept("com.bbf.redis");
    return parserConfig;
  }
}
