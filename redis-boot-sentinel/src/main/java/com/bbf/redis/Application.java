package com.bbf.redis;

import com.bbf.redis.util.FastJson2JsonRedisSerializer;
import com.bbf.redis.util.FastJsonAutoTypeConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

/**
 * 入口类
 * @author BBF
 */
@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }

  @Bean(name = "stringRedisTemplate")
  public StringRedisTemplate stringRedisTemplate(RedisConnectionFactory redisConnectionFactory) {
    StringRedisTemplate template = new StringRedisTemplate(redisConnectionFactory);
    //value的序列化使用fastJson
    template.setValueSerializer(new FastJson2JsonRedisSerializer<>(Object.class));
    //key的序列化使用StringRedisSerializer，字符串编码数据以String存储
    template.setKeySerializer(new StringRedisSerializer());
    template.afterPropertiesSet();
    return template;
  }

  /**
   * 添加FastJson的autoType白名单
   * @return ParserConfig
   * @see <a href="https://github.com/alibaba/fastjson/wiki/enable_autotype">添加autoType白名单</a>
   */
  @Bean
  public FastJsonAutoTypeConfig fastJsonAutoTypeConfig() {
    return new FastJsonAutoTypeConfig("com.bbf.redis", "com.bbf.redis");
  }
}
