# Spring_Redis_Test
使用``SpringBoot``框架，构建``Redis``的示例程序。 

其中包括``SpringMVC``的``xml``文件模式和``Redis``的主从``Sentinel``的配置。    
 Redis使用Redis-x64-3.2.100（win）

使用须知：    
``redis_porc``中存放着配置好的``redis``主从设置。    

## Redis普通模式
使用startup.bat开启普通单点模式，适用于redis-boot和redis-boot-mvc。**127.0.0.1:6397，无密码**    

## Redis主从模式
先启动Redis主从服务，运行run_srv_all.bat，也可以单独运行run_src_7xxx.bat。    
再启动Sentinel服务，运行run_sentinel.bat。    
适用于redis-boot-sentinel和redis-boot-mvc-sentinel。**127.0.0.1:7000,127.0.0.1:7001,127.0.0.1:7002 ; 密码bbf**

## 测试
执行各自项目内的WebControllerTest.java的单元测试。
