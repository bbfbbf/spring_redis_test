package com.bbf.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

/**
 * 入口类
 * @author BBF
 */
@SpringBootApplication
@ImportResource(locations = {"classpath:spring-conf-redis.xml"})
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
